package com.booking.app.controllers;

import com.booking.app.domain.models.Booking;
import com.booking.app.services.ServiceBookingCollection;

import java.io.IOException;
import java.util.List;


public class ControllerBookingCollection {
    ServiceBookingCollection serviceBookingCollection;
    public ControllerBookingCollection(ServiceBookingCollection serviceBookingCollection) {
        this.serviceBookingCollection = serviceBookingCollection;
    }


    public List<Booking> getBookingCollectionFromDB() {
        return serviceBookingCollection.getBookingCollectionFromDB();
    }


    public List<Booking> getBookingCollection(){
        return serviceBookingCollection.getBookingCollection();
    }


    public Booking getBookingByIndex(int index) {
        return serviceBookingCollection.getBookingByIndex(index);
    }


    public boolean deleteBookingByIndex(int index) {
        return serviceBookingCollection.deleteBookingByIndex(index);
    }


    public Booking updateBookingCollection(Booking booking) {
        return serviceBookingCollection.updateBookingCollection(booking);
    }


    public boolean saveBookingCollectionToDB(List<Booking> bookingCollection) {
        return serviceBookingCollection.saveBookingCollectionToDB(bookingCollection);
    }



// Additional DaoBookingCollection Methods
    public boolean setBookingCollection(List<Booking> bookingCollection) {
        return serviceBookingCollection.setBookingCollection(bookingCollection);
    }


    public void showBookingCollection(List<Booking> bookingCollection) {
        serviceBookingCollection.showBookingCollection(bookingCollection);
    }



// Additional ServiceBookingCollection Methods
    public boolean deleteBookingByID(int bookingID){
        return serviceBookingCollection.deleteBookingByID(bookingID);
    }


    public List<Booking> getBookingListOfCertainPassenger(String name, String surName){
        return serviceBookingCollection.getBookingListOfCertainPassenger(name, surName);
    }


    public Booking getBookingByBookingID(int bookingID){
        return serviceBookingCollection.getBookingByBookingID(bookingID);
    }

}