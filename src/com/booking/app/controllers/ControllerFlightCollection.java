package com.booking.app.controllers;

import com.booking.app.services.ServiceFlightCollection;
import com.booking.app.domain.models.Flight;

import java.io.IOException;
import java.util.*;


public class ControllerFlightCollection {
    ServiceFlightCollection serviceFlightCollection;
    public ControllerFlightCollection(ServiceFlightCollection serviceFlightCollection){
        this.serviceFlightCollection = serviceFlightCollection;
    }


    public List<Flight> getFlightCollectionFromDB() {
        return serviceFlightCollection.getFlightCollectionFromDB();
    }


    public List<Flight> getFlightCollection(){
        return serviceFlightCollection.getFlightCollection();
    }


    public Flight getFlightByIndex(int index) {
        return  serviceFlightCollection.getFlightByIndex(index);
    }


    public boolean deleteFlightByIndex(int index) {
        return serviceFlightCollection.deleteFlightByIndex(index);
    }


    public Flight updateFlightCollection(Flight flight) {
        return serviceFlightCollection.updateFlightCollection(flight);
    }


    public boolean saveFlightCollectionToDB(List<Flight> flightCollection) {
        return serviceFlightCollection.saveFlightCollectionToDB(flightCollection);
    }



// Additional DaoFlightCollection Methods
    public List<Flight> generateFlightCollectionDB(int flightsNumber, int forNextNumberDays){
        return serviceFlightCollection.generateFlightCollectionDB(flightsNumber, forNextNumberDays);
    }


    public boolean setFlightCollection(List<Flight> collection) {
        return serviceFlightCollection.setFlightCollection(collection);
    }



// Additional ServiceFlightCollection Methods
    public void showFlightCollection(List<Flight> collection) {
        serviceFlightCollection.showFlightCollection(collection);
    }


    public List<Flight> sortFlightsByDate(List<Flight> flightsCollection){
        return serviceFlightCollection.sortFlightsByDate(flightsCollection);
    }


    public List<Flight> sortFlightsByDestination(List<Flight> flightsCollection){
        return serviceFlightCollection.sortFlightsByDestination(flightsCollection);
    }


    public List<Flight> getFlightsInOneDayPeriod(){
        return serviceFlightCollection.getFlightsInOneDayPeriod();
    }


    public Flight getFlightByID(int flightID){
        return serviceFlightCollection.getFlightByFlightID(flightID);
    }


    public List<Flight> findFlights(String destination, String date, int numberOfPassenger){
        return serviceFlightCollection.findFlights(destination, date, numberOfPassenger);
    }

}