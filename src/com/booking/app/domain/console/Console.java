package com.booking.app.domain.console;

import com.booking.app.dao.DaoFlightCollection;
import com.booking.app.dao.DaoBookingCollection;
import com.booking.app.services.ServiceFlightCollection;
import com.booking.app.services.ServiceBookingCollection;
import com.booking.app.controllers.ControllerFlightCollection;
import com.booking.app.controllers.ControllerBookingCollection;

import com.booking.app.domain.models.Flight;
import com.booking.app.domain.models.Booking;
import com.booking.app.domain.models.Passenger;
import com.booking.app.domain.models.PassengerCollection;

import com.booking.app.domain.console.consoleController.ConsoleController;
import com.booking.app.domain.console.wrongInputDataException.WrongInputDataException;

import java.util.*;


public class Console {
    private final ControllerFlightCollection controllerFlightCollection;
    private final ControllerBookingCollection controllerBookingCollection;

    private final List<String> mainMenuOfBookingApp;
    private final ConsoleController consoleController;
    Scanner scanner = new Scanner(System.in);

    public Console(){
        this.controllerFlightCollection = new ControllerFlightCollection(new ServiceFlightCollection(new DaoFlightCollection()));
        this.controllerBookingCollection = new ControllerBookingCollection(new ServiceBookingCollection(new DaoBookingCollection()));

        List<Flight> flightCollection = controllerFlightCollection.getFlightCollectionFromDB();
        System.out.println("Flight Collection contains " + flightCollection.size() + " flights\n");

        List<Booking> bookingCollection = controllerBookingCollection.getBookingCollectionFromDB();
        System.out.println("Booking Collection contains " + bookingCollection.size() + " bookings");
        if(bookingCollection.size() == 0)
            controllerBookingCollection.saveBookingCollectionToDB(controllerBookingCollection.getBookingCollection());

        if(flightCollection.size() == 0){
            System.out.println("\n>>> Generating Flight Collection!");
            flightCollection = controllerFlightCollection.generateFlightCollectionDB(3000, 30);

            controllerFlightCollection.setFlightCollection(controllerFlightCollection.sortFlightsByDate(flightCollection));

            System.out.println("New Flight Collection contains " + controllerFlightCollection.getFlightCollection().size() + " flights");
            controllerFlightCollection.saveFlightCollectionToDB(controllerFlightCollection.getFlightCollection());
        }

        mainMenuOfBookingApp = new ArrayList<>();
        consoleController = new ConsoleController();
        fillMainMenuOptions();
    }


//  Метод fillMainMenuOptions() - заполняет поле экземпляра класса Console List<String> mainMenuOfBookingApp в конструкторе
    private void fillMainMenuOptions(){
        mainMenuOfBookingApp.add("0  .....>>  View all flights from Kiev");
        mainMenuOfBookingApp.add("1  .....>>  View the flights from Kiev in the next 24 hours");
        mainMenuOfBookingApp.add("2  .....>>  View flight information by flight ID");
        mainMenuOfBookingApp.add("3  .....>>  Search for flights and Book the flights");
        mainMenuOfBookingApp.add("4  .....>>  Cancel booking by it's ID");
        mainMenuOfBookingApp.add("5  .....>>  View list of all bookings of certain passenger");
        mainMenuOfBookingApp.add("6  .....>>  View list of all bookings");
        mainMenuOfBookingApp.add("exit  ..>>  Quit the application");
    }


//  Метод run() - запускает работу экземпляра класса Console
    public void run(){
        while (true){
            showMainMenuOfBookingApp();
            try {
                implementTheSelectedActionOfMainMenu();
            } catch (WrongInputDataException e){
                WrongInputDataException.throwException();
            }
        }
    }


//  Метод showMainMenuOfBookingApp() - выводит главное меню приложения booking в консоль
    private void showMainMenuOfBookingApp(){
        System.out.println("\nSelect the option of the main menu:");
        mainMenuOfBookingApp.forEach(System.out::println);
    }


//  implementTheSelectedActionOfMainMenu() - (реализует) вызывает методы для реализации выбранного пользователем пункта меню
    private void implementTheSelectedActionOfMainMenu() {
        System.out.print("Enter your choice, available option: [ 0 | 1 | 2 | 3 | 4 | 5 | 6 | exit ] >>> ");
        String userChoice = scanner.nextLine().toLowerCase().trim();
        switch (userChoice){
            case "0" -> showAllFlightsCollection();
            case "1" -> showOnlineFlightsScoreboard();
            case "2" -> showFlightInfoById();
            case "3" -> searchFlightAndBook();
            case "4" -> cancelBookingById();
            case "5" -> showAllBookingsOfCertainPassenger();
            case "6" -> showAllBookingsCollection();
            case "exit" -> exit();
            default -> throw new WrongInputDataException();
        }
    }


//  Метод exit() - завершает работу экземпляра класса Console
    private void exit() {
        controllerFlightCollection.saveFlightCollectionToDB(controllerFlightCollection.sortFlightsByDate(controllerFlightCollection.getFlightCollection()));
        controllerBookingCollection.saveBookingCollectionToDB(controllerBookingCollection.getBookingCollection());
        scanner.close();
        System.out.println("\nExit the application. Goodbye!");
        System.exit(1);
    }


//  Метод showAllFlightsCollection() - показывает информацию про все рейсы из List<Flight> flightCollection
    private void showAllFlightsCollection() {
        System.out.println("\nView all flights from Kiev >>> ");
        System.out.println("\nAll Flight from Kiev in flightCollection DB:");
        controllerFlightCollection.showFlightCollection(controllerFlightCollection.getFlightCollection());
    }


//  Метод showOnlineFlightScoreboard() - показывает информацию про все рейсы из Киева в ближайшие 24 часа из List<Flight> flightCollection
    private void showOnlineFlightsScoreboard(){
        System.out.println("\n>>> View the flights from Kiev in the next 24 hours");
        System.out.println("\nThe Online Scoreboard Flight:");
        List<Flight> flights = controllerFlightCollection.getFlightsInOneDayPeriod();
        flights = controllerFlightCollection.sortFlightsByDestination(flights);
        controllerFlightCollection.showFlightCollection(flights);
    }


//  Метод showFlightInfoById() - показывает информацию по выбранному по ID(flightID) из DB flightCollection рейсу
    private void showFlightInfoById(){
        System.out.println("\n>>> View flight information by flight ID");
        int flightID = -1;

        while (flightID == -1){
            System.out.print("Enter flight ID of Flight you'd like to get information about, required: [integer only [100000 : 999999]] >>> ");
            flightID = consoleController.checkInputDataInteger(scanner.nextLine().toLowerCase().trim(), 100000, 999999);
        }

        Flight flight = controllerFlightCollection.getFlightByID(flightID);
        if(flight != null){
            System.out.println("\nFound Flight with flight ID='" + flightID + "'. Information about:");
            flight.prettyFormatFlightFullInfo();
        } else {
            System.out.println("\nThere is No Flight Found with flight ID='" + flightID + "'");
        }
    }


//  Метод searchFlightAndBook() - предоставляет возможность выбрать определенные рейсы и забронтровать на определенный рейс билеты
//  если добро на бронь - добавляет booking(бронь) в bookingCollection, - апдейтит flightCollection по рейсу (SoldPlaces, AvailablePlaces)
    private void searchFlightAndBook(){
        System.out.println("\n>>> Search and Book the flights");
        String destinationStr = "", dateStr = "";
        int ticketsNumber = -1;

        System.out.println("To search for flights, please enter some data:");
        while (destinationStr.equals("")){
            System.out.print("Enter flight arrival destination, required: [characters only] >>> ");
            destinationStr = consoleController.checkInputDataChars(scanner.nextLine().toLowerCase());
        }
        destinationStr = consoleController.toUpperCaseFirstLetterEachWorld(destinationStr);

        while (dateStr.equals("")){
            System.out.print("Enter the departure date of flights, required: [dd/MM/yyyy] >>> ");
            dateStr = consoleController.checkInputDataDateString(scanner.nextLine().toLowerCase().trim());
        }

        while (ticketsNumber == -1){
            System.out.print("Enter number of tickets you'd like to book, required: [integer only, 1 to 5 tickets available in one booking] >>> ");
            ticketsNumber = consoleController.checkInputDataInteger(scanner.nextLine().toLowerCase().trim(), 1, 5);
        }

        List<Flight> foundFlights = controllerFlightCollection.findFlights(destinationStr, dateStr, ticketsNumber);
        if(foundFlights.size() > 0){
            System.out.printf("\nFound Flights List to %s,\t on date %s, \twith the required number (%d) of available tickets: \n",
                    destinationStr, dateStr, ticketsNumber);
            controllerFlightCollection.showFlightCollection(foundFlights);
            System.out.println();

            int listSize = foundFlights.size();
            int userChoice = -1;
            while (userChoice == -1){
                System.out.print("Enter 0 to go back to the main menu, OR " +
                        "\nEnter the number (NOT ID!) of flight from shown Found Flights List to continue booking, required: [1:" + listSize  + "] >>> ");
                userChoice = consoleController.checkInputDataInteger(scanner.nextLine().toLowerCase().trim(), 0, listSize);
                if(userChoice == 0) {
                    System.out.println("Exit booking!");
                    break;
                }
            }

            if(userChoice > 0){
                Flight flight = foundFlights.get(userChoice-1);

                PassengerCollection passengersList = createPassengerList(ticketsNumber);
                Booking booking = new Booking(flight, passengersList);

                int bookingID = controllerBookingCollection.updateBookingCollection(booking).getBookingID();
                if(bookingID != -1){
                    controllerBookingCollection.saveBookingCollectionToDB(controllerBookingCollection.getBookingCollection());

                    flight.setSoldPlaces(flight.getSoldPlaces() + ticketsNumber);
                    flight.setAvailablePlaces();
                    controllerFlightCollection.updateFlightCollection(flight);
                    controllerFlightCollection.saveFlightCollectionToDB(controllerFlightCollection.getFlightCollection());

                    System.out.print("\nBooking Done!\n\t");
                    booking.prettyFormatBookingFullInfo();
                } else {
                    System.out.println("\nBooking Fail! Try again!");
                }
            }
        } else { System.out.printf("%nFlights by your request Not Found!" +
                "%nThere is no flights to %s, on date %s, with the required number (%d) of available tickets.%n"
                , destinationStr, dateStr, ticketsNumber); }

    }


//  Метод createPassengerList() - формирует коллекцию пассажиров для брони билетов на рейс
    private PassengerCollection createPassengerList(int ticketsNumber){
        System.out.print("\n>>> Continue booking!");
        PassengerCollection passengersList = new PassengerCollection();
        for(int i = 1; i <= ticketsNumber; i++){
            System.out.println();
            String surName = "", name = "";

            while (name.equals("")){
                System.out.printf("Enter the Name of passenger %d, required: [characters only] >>> ", i);
                name = consoleController.checkInputDataChars(scanner.nextLine().toLowerCase());
            }
            name = consoleController.toUpperCaseFirstLetterEachWorld(name);

            while (surName.equals("")){
                System.out.printf("Enter the Last Name of passenger %d, required: [characters only] >>> ", i);
                surName = consoleController.checkInputDataChars(scanner.nextLine().toLowerCase());
            }
            surName = consoleController.toUpperCaseFirstLetterEachWorld(surName);

            Passenger passenger = new Passenger(name, surName);
            passengersList.addPassenger(passenger);
        }
        return passengersList;
    }


//  Метод cancelBookingById() - отменяет (удаляет) бронь из DB bookingCollection по ID брони
//  - апдейтит flightCollection по рейсу (SoldPlaces, AvailablePlaces)
    private void cancelBookingById(){
        System.out.println("\n>>> Cancel flight booking by it's ID!");
        int bookingID = -1;

        while (bookingID == -1){
            System.out.print("Enter booking ID of Booking you'd like to cancel, required: [integer only [100000 : 999999]] >>> ");
            bookingID = consoleController.checkInputDataInteger(scanner.nextLine().toLowerCase().trim(), 100000, 999999);
        }

        Booking booking = controllerBookingCollection.getBookingByBookingID(bookingID);
        if(booking != null){
            int seatsInBooking = booking.getNumberOfSeats();
            Flight flight = booking.getFlight();

            if(controllerBookingCollection.deleteBookingByID(bookingID)){
                flight.setSoldPlaces(flight.getSoldPlaces() - seatsInBooking);
                flight.setAvailablePlaces();

                controllerFlightCollection.updateFlightCollection(flight);
                controllerFlightCollection.saveFlightCollectionToDB(controllerFlightCollection.getFlightCollection());

                controllerBookingCollection.saveBookingCollectionToDB(controllerBookingCollection.getBookingCollection());
                System.out.println("\nThe Booking with booking ID='" + bookingID + "' was canceled!");
            }

        } else {
            System.out.println("\nThere is No Booking Found with booking ID='" + bookingID + "'");
        }
    }


//  Метод showAllBookingOfCertainPassenger() - находит (по имени и фамилии пассажира) его брони и выводит их в консоль
    private void showAllBookingsOfCertainPassenger(){
        System.out.println("\n>>> View list of all bookings of certain passenger");
        String surName = "", name = "";

        while (name.equals("")){
            System.out.print("Enter the Name of passenger, required: [characters only] >>> ");
            name = consoleController.checkInputDataChars(scanner.nextLine().toLowerCase());
        }
        name = consoleController.toUpperCaseFirstLetterEachWorld(name);

        while (surName.equals("")){
            System.out.print("Enter the Last Name of passenger, required: [characters only] >>> ");
            surName = consoleController.checkInputDataChars(scanner.nextLine().toLowerCase());
        }
        surName = consoleController.toUpperCaseFirstLetterEachWorld(surName);

        List<Booking> bookings = controllerBookingCollection.getBookingListOfCertainPassenger(name, surName);

        if(bookings.size() != 0){
            int i = 1;
            System.out.printf("\nBookings for passenger %s %s:%n", name, surName);
            for(Booking booking : bookings){
                System.out.println("#" + i + "  >>> ");
                booking.prettyFormatBookingFullInfo();
                i++;
            }
        } else {
            System.out.printf("\nThere is No Bookings for passenger %s %s!%n", name, surName);
        }
    }


//  Метод showAllBookingsCollection() - показывает информацию про все брони из List<Booking> bookingCollection
    public void showAllBookingsCollection(){
        System.out.println("\nView list of all bookings >>> ");
        System.out.println("\nAll Bookings in bookingCollection DB:");
        if(controllerBookingCollection.getBookingCollection().size() == 0){
            System.out.println("There is No any Booking in bookingCollection for now!");
        } else{
            controllerBookingCollection.showBookingCollection(controllerBookingCollection.getBookingCollection());
        }
    }

}