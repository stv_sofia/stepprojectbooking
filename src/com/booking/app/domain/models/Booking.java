package com.booking.app.domain.models;

import java.io.Serializable;
import java.util.*;


public class Booking implements Serializable {
    private int bookingID = -1;
    private Flight flight;
    private PassengerCollection passengers;
    private int numberOfSeats = -1;

    public Booking(){}
    public Booking(Flight flight, PassengerCollection passengers) {
        this.bookingID = generateBookingId();
        this.flight = flight;
        this.passengers = passengers;
        setNumberOfSeats();
    }

    public int getBookingID() {
        return bookingID;
    }
    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public Flight getFlight() {
        return flight;
    }
    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public PassengerCollection getPassengers() {
        return passengers;
    }
    public void setPassengers(PassengerCollection passengers) {
        this.passengers = passengers;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }
    public void setNumberOfSeats() {
        this.numberOfSeats = getPassengers().getPassengerCollection().size();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Booking)) return false;
        Booking booking = (Booking) o;
        return getBookingID() == booking.getBookingID()
                && getNumberOfSeats() == booking.getNumberOfSeats()
                && Objects.equals(getFlight(), booking.getFlight())
                && Objects.equals(getPassengers(), booking.getPassengers());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBookingID(), getFlight(), getPassengers(), getNumberOfSeats());
    }

    @Override
    public String toString() {
        return "Booking{" +
                "bookingID=" + bookingID +
                ", flight=" + flight +
                ", passengers=" + passengers +
                ", numberOfSeats=" + numberOfSeats +
                '}';
    }


    public void prettyFormatBookingFullInfo() {
        if(getBookingID() != -1 && getFlight() != null && getPassengers().getPassengerCollection().size() != 0){
            String format = "%-26s%n";
            String booking = "Booking: " + "bookingID='" + this.getBookingID() + "'";
            System.out.printf(format, booking);
            getFlight().prettyFormatFlight();
            getPassengers().prettyFormatPassengerCollection();
            System.out.println("\tNumber of seats: " + getNumberOfSeats() + "\n");
        }
    }


    private int generateBookingId(){
        return (int) (100000 + (Math.random() * 900000));
    }


    public int getIDBookingIfPassengerPresent(String name, String surName){
        int bookingIDToReturn = -1;
        for(Passenger passenger : getPassengers().getPassengerCollection()){
            if(passenger.getName().equals(name) && passenger.getSurName().equals(surName)){
                bookingIDToReturn = this.bookingID;
                break;
            }
        }
        return bookingIDToReturn;
    }

}