package com.booking.app.domain.models;

import java.io.Serializable;
import java.util.Objects;

public class Passenger implements Serializable {
    private int passengerID = -1;
    private String name = "";
    private  String surName = "";

    public Passenger(){}
    public Passenger(String name, String surName){
        this.passengerID = generatePassengerId();
        this.name = name;
        this.surName = surName;
    }

    public int getPassengerID() {
        return passengerID;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }
    public void setSurName(String surName) {
        this.surName = surName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Passenger)) return false;
        Passenger passenger = (Passenger) o;
        return getPassengerID() == passenger.getPassengerID() && getName().equals(passenger.getName()) && getSurName().equals(passenger.getSurName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPassengerID(), getName(), getSurName());
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "passengerID=" + passengerID +
                ", name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                '}';
    }


    public void prettyFormatPassenger() {
        String format = "%-28s%s%n";
        String name = "\tPassenger:  name='" + (this.getName().equals("") ? "no info" : this.getName()) + "'";
        String surName = "  last name='" + (this.getSurName().equals("") ? "no info" : this.getSurName()) + "'";
        System.out.printf(format, name, surName);
    }

    public void prettyFormatPassengerFullInfo() {
        String format = "%-24s%-20s%s%n";
        String passenger = "\tPassenger: " + "passengerID='" + (this.getPassengerID() == -1 ? "no info" : this.getPassengerID()) + "'";
        String name = " name='" + (this.getName().equals("") ? "no info" : this.getName()) + "'";
        String surName = " last name='" + (this.getSurName().equals("") ? "no info" : this.getSurName()) + "'";
        System.out.printf(format, passenger, name, surName);
    }

    private int generatePassengerId(){
        return (int) (1000 + (Math.random() * 9000));
    }
}