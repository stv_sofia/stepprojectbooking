package com.booking.app.domain.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PassengerCollection implements Serializable {
    private List<Passenger> passengerCollection;

    public PassengerCollection(){
        this.passengerCollection = new ArrayList<>();
    }

    public PassengerCollection(List<Passenger> passengerCollection) {
        this.passengerCollection = passengerCollection;
    }

    public List<Passenger> getPassengerCollection() {
        return passengerCollection;
    }
    public void setPassengerCollection(List<Passenger> passengerCollection) {
        this.passengerCollection = passengerCollection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PassengerCollection)) return false;
        PassengerCollection that = (PassengerCollection) o;
        return Objects.equals(getPassengerCollection(), that.getPassengerCollection());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPassengerCollection());
    }

    @Override
    public String toString() {
        return "PassengerCollection{" +
                "passengerCollection=" + passengerCollection +
                '}';
    }


    public void prettyFormatPassengerCollection() {
        getPassengerCollection().forEach(Passenger::prettyFormatPassenger);
    }

    public void addPassenger(Passenger passenger){
        getPassengerCollection().add(passenger);
    }

    public Passenger getPassengerByID(int passengerID){
        return getPassengerCollection().get(passengerID);
    }

    public Passenger getPassengerByNameAndSurName(String name, String surName){
        for (Passenger passenger : getPassengerCollection()) {
            if (passenger.getName().equals(name) && passenger.getSurName().equals(surName)){
                return passenger;
            }
        }
        return null;
    }

}