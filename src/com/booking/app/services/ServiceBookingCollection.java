package com.booking.app.services;

import com.booking.app.dao.DaoBookingCollection;
import com.booking.app.domain.models.Booking;

import java.util.List;
import java.util.stream.Collectors;


public class ServiceBookingCollection {
    private final DaoBookingCollection daoBookingCollection;
    public ServiceBookingCollection(DaoBookingCollection daoBookingCollection){
        this.daoBookingCollection = daoBookingCollection;
    }

    //(dao abstract)
    public List<Booking> getBookingCollectionFromDB(){
        return daoBookingCollection.getCollectionFromDB();
    }


    //(dao abstract)
    public List<Booking> getBookingCollection(){
        return daoBookingCollection.getAllCollection();
    }


    //(dao abstract)
    public Booking getBookingByIndex(int index) {
        return daoBookingCollection.getByIndex(index);
    }


    //(dao abstract)
    public boolean deleteBookingByIndex(int index) {
        return daoBookingCollection.deleteByIndex(index);
    }


    //(dao abstract)
    public Booking updateBookingCollection(Booking booking) {
        return daoBookingCollection.updateCollection(booking);
    }


    //(dao abstract)
    public boolean saveBookingCollectionToDB(List<Booking> bookingCollection) {
        return daoBookingCollection.saveCollectionToDB(bookingCollection);
    }



// Additional DaoBookingCollection Methods
    public boolean setBookingCollection(List<Booking> bookingCollection) {
        return daoBookingCollection.setCollection(bookingCollection);
    }


    public void showBookingCollection(List<Booking> bookingCollection) {
        daoBookingCollection.showBookingCollection(bookingCollection);
    }



// Additional ServiceBookingCollection Methods
    public boolean deleteBookingByID(int bookingID){
        return getBookingCollection().removeIf(booking -> booking.getBookingID() == bookingID);
    }


    public List<Booking> getBookingListOfCertainPassenger(String name, String surName){
        return getBookingCollection().stream().filter
                (booking -> (booking.getBookingID() == booking.getIDBookingIfPassengerPresent(name, surName)))
                .collect(Collectors.toList());
    }


    public Booking getBookingByBookingID(int bookingID){
        return daoBookingCollection.getBookingByBookingID(bookingID);
    }

}