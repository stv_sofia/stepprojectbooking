package com.booking.app.services;

import com.booking.app.dao.DaoFlightCollection;
import com.booking.app.domain.models.Flight;

import java.time.*;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ServiceFlightCollection {
    private final DaoFlightCollection daoFlightCollection;
    public ServiceFlightCollection(DaoFlightCollection daoFlightCollection){
        this.daoFlightCollection = daoFlightCollection;
    }

    //(dao abstract)
    public List<Flight> getFlightCollectionFromDB() {
        return daoFlightCollection.getCollectionFromDB();
    }


    //(dao abstract)
    public List<Flight> getFlightCollection(){
        return daoFlightCollection.getAllCollection();
    }


    //(dao abstract)
    public Flight getFlightByIndex(int index) {
        return daoFlightCollection.getByIndex(index);
    }


    //(dao abstract)
    public boolean deleteFlightByIndex(int index) {
        return daoFlightCollection.deleteByIndex(index);
    }


    //(dao abstract)
    public Flight updateFlightCollection(Flight flight) {
        return daoFlightCollection.updateCollection(flight);
    }


    //(dao abstract)
    public boolean saveFlightCollectionToDB(List<Flight> flightCollection) {
        return daoFlightCollection.saveCollectionToDB(flightCollection);
    }




// Additional DaoFlightCollection Methods
    public List<Flight> generateFlightCollectionDB(int flightsNumber, int forNextNumberDays){
        return daoFlightCollection.generateFlightCollectionDB(flightsNumber, forNextNumberDays);
    }


    public boolean setFlightCollection(List<Flight> collection) {
        return daoFlightCollection.setCollection(collection);
    }



// Additional ServiceFlightCollection Methods
    public void showFlightCollection(List<Flight> collection) {
        int index = 1;
        for(Flight flight : collection){
            System.out.printf("#%-3d", index);
            flight.prettyFormatFlight();
            index++;
        }
    }


    public List<Flight> sortFlightsByDate(List<Flight> flightsCollection){
        return flightsCollection.stream()
                .sorted((o1, o2) -> (int) (o1.getDateSeconds() - o2.getDateSeconds()))
                .collect(Collectors.toList());
    }


    public List<Flight> sortFlightsByDestination(List<Flight> flightsCollection){
        return flightsCollection.stream()
                .sorted(Comparator.comparing(Flight::getDestination))
                .collect(Collectors.toList());
    }


    public Flight getFlightByFlightID(int flightID){
        for (Flight flight: getFlightCollection()) {
            if(flight.getFlightID() == flightID)
                return flight;
        }
        return null;
    }


    public List<Flight> getFilteredFlight(Predicate<Flight> predicate) {
        return getFlightCollection().stream().filter(predicate).collect(Collectors.toList());
    }


    public List<Flight> getFlightsInOneDayPeriod(){
        List<Flight> filteredFlight = this.getFilteredFlight(flight ->
                        LocalDateTime.ofEpochSecond(flight.getDateSeconds(), 0, ZoneOffset.UTC)
                            .isAfter(LocalDateTime.now()) &&
                        LocalDateTime.ofEpochSecond(flight.getDateSeconds(), 0, ZoneOffset.UTC)
                            .isBefore(LocalDateTime.now().plusDays(1)));
       return sortFlightsByDate(filteredFlight);
    }


    public List<Flight> findFlights(String destination, String date, int numberOfPassenger){
        List<Flight> filteredFlight = this.getFilteredFlight( flight -> (flight.getDestination().equals(destination)
                && flight.getDate().equals(date) && flight.getAvailablePlaces() >= numberOfPassenger) );
        return sortFlightsByDate(filteredFlight);
    }

}