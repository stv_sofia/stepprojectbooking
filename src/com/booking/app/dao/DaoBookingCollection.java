package com.booking.app.dao;

import com.booking.app.domain.models.Booking;

import java.io.Serializable;
import java.util.*;


public class DaoBookingCollection extends DaoAbstract<Booking> implements Serializable {

    public DaoBookingCollection() {
        super("bookingCollectionDB.data");
    }

    public DaoBookingCollection(String path) {
        super(path);
    }



// Additional DaoBookingCollection Methods
    //  Выводит в консоль коллекцию букинг (всю или отфильтрованную часть) из поля List<Booking>
    public void showBookingCollection(List<Booking> collection) {
        int index = 1;
        for(Booking flight : collection){
            System.out.printf("#%-3d", index);
            flight.prettyFormatBookingFullInfo();
            index++;
        }
    }


    //  Возвращает букинг из коллекции BookingCollection по его полю bookingID
    public Booking getBookingByBookingID(int bookingID){
        for(Booking booking : super.getAllCollection()){
            if(booking.getBookingID() == bookingID){
                return booking;
            }
        }
        return null;
    }

}