package com.booking.app.dao;

import com.booking.app.domain.models.Flight;

import java.util.List;


public interface Dao<T> {
    List<T> getCollectionFromDB();

    List<T> getAllCollection();

    T getByIndex(int index);

    boolean deleteByIndex(int index);

    T updateCollection(T element);

    boolean saveCollectionToDB(List<T> collection);
}