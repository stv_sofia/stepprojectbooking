package com.booking.app.dao;

import java.util.*;
import com.booking.app.domain.serializator.Serializator;


public abstract class DaoAbstract<T> implements Dao<T>{
    String filePath;
    List<T> collection;

    public DaoAbstract() {}
    public DaoAbstract(String filePath) {
        this.filePath = filePath;
        this.collection = new ArrayList<T>();
    }

    public List<T> getCollection() {
        return this.collection;
    }
    public boolean setCollection(List<T> collection) {
        this.collection = collection;
        return collection.size()!= 0;
    }

    public String getFilePath() {
        return this.filePath;
    }
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    //  Считывает и устанавливает объекты коллекции из файла по пути из поля String filePath
    //  - устанавливает коллекцию в поле List<T> collection
    //  - и возвращает список всех эл-в из поля List<T> collection // - если данных нет List<T> collection = new ArrayList<T>()
    @Override
    public List<T> getCollectionFromDB() {
        String ANSI_RED = "\u001B[31m", ANSI_RESET = "\u001B[0m";

        Serializator serializator = new Serializator();
        List<T> collectionDB = null;
        try{
            collectionDB = serializator.deSerialization(getFilePath());
            if(collectionDB != null){
                setCollection(collectionDB);
                return collectionDB;
            }
            System.out.println(ANSI_RED + "Collection DB is empty!" + ANSI_RESET);
        } catch (ClassNotFoundException e) {
            System.out.println(ANSI_RED + e.getMessage() + ANSI_RESET);
        }
        return getCollection();
    }


    //  Возвращает коллекцию всех эл-в рейсов из поля List<T> collection
    @Override
    public List<T> getAllCollection() {
        return getCollection();
    }


    //  Возвращает эл-т из поля коллекции List<T> collection по его индексу (порядковому номеру) в коллекции
    @Override
    public T getByIndex(int index) {
        if(collection.contains(collection.get(index)))
            return collection.get(index);
        return null;
    }


    //  Удаляет эл-т из поля коллекции List<T> collection по его индексу (порядковому номеру) в коллекции
    @Override
    public boolean deleteByIndex(int index) {
        String ANSI_RED = "\u001B[31m", ANSI_RESET = "\u001B[0m";
        try {
            collection.contains(collection.get(index));
            collection.remove(index);
            return true;
        } catch (IndexOutOfBoundsException e){
            System.out.println("\n" + ANSI_RED + e.getMessage() + ANSI_RESET);
            System.out.println(ANSI_RED + "There is no such element in collection!" + ANSI_RESET);
            return false;
        }
    }


    //  Апдейтид объект <T> коллекции из поля коллекции List<T> collection, либо записывает объект в конец коллекции
    @Override
    public T updateCollection(T element){
        if(collection.contains(element)){
            collection.set(collection.indexOf(element), element);
            return collection.get(collection.indexOf(element));
        }
        collection.add(element);
        return collection.get(collection.indexOf(element));
    }


    //  Записывает объекты коллекции из поля коллекции List<T> collection в файл по пути из поля String filePath
    @Override
    public boolean saveCollectionToDB(List<T> collection) {
        Serializator serializator = new Serializator();
        return serializator.serialization(getCollection(), getFilePath());
    }

}