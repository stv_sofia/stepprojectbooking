package com.booking.app.dao;

import com.booking.app.domain.models.Flight;
import com.booking.app.domain.flightGenerator.FlightGenerator;

import java.io.Serializable;
import java.util.*;

public class DaoFlightCollection extends DaoAbstract<Flight> implements Serializable {

    public DaoFlightCollection() {
        super("flightCollectionDB.data");
    }

    public DaoFlightCollection(String path) {
        super(path);
    }



// Additional DaoFlightCollection Methods
    //  Если коллекция не считывается из getCollectionDB() >> значит еще не записана в файл (в DB) >>>
    //  Генерирует коллекцию рейсов (int flightsNumber экземпляров) на ближайшие (int forNextNumberDays) дней
    public List<Flight> generateFlightCollectionDB(int flightsNumber, int forNextNumberDays){
        return FlightGenerator.generateFlightCollection(flightsNumber, forNextNumberDays);
    }

    public boolean setFlightCollection(List<Flight> collection) {
        return super.setCollection(collection);
    }
}