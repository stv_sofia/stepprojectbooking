package com.booking.app;

import com.booking.app.domain.console.Console;
import com.booking.app.domain.console.wrongInputDataException.WrongInputDataException;


public class App {
    public static void main(String[] args) throws WrongInputDataException {
        System.out.println("Welcome to the best flight booking app!\n");
        Console console = new Console();
        console.run();
    }
}