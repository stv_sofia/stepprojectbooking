package com.booking.app.test;

import com.booking.app.dao.DaoFlightCollection;
import com.booking.app.domain.flightGenerator.FlightGenerator;
import com.booking.app.domain.models.Flight;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.*;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

// !!! Before starting tests need to delete file .testFlightCollectionDB.data if exist
class DaoFlightCollectionTest{

    DaoFlightCollection daoFlightCollection = new DaoFlightCollection("testFlightCollectionDB.data");
    List<Flight> flights = new ArrayList<>();


    @BeforeEach
    void setUp() {
        LocalDateTime dateTime = LocalDateTime.now();
        Flight flight1 = new Flight(1111, "Paris",
                dateTime.toEpochSecond(ZoneOffset.UTC), 20, 10);
        Flight flight2 = new Flight(2222, "London",
                dateTime.plusDays(10).toEpochSecond(ZoneOffset.UTC), 20, 18);
        Flight flight3 = new Flight(3333, "Rim",
                dateTime.plusDays(2).toEpochSecond(ZoneOffset.UTC), 20, 8);
        flights.add(flight1); flights.add(flight2); flights.add(flight3);
    }



    @Test
    void checkGetCollectionFromDBFileNotFoundSuccess(){
        List<Flight> flightCollection = daoFlightCollection.getCollectionFromDB();
        assertEquals(0, flightCollection.size());
    }



    @Test
    void checkGetCollectionFromDBFileFoundSuccess(){
        daoFlightCollection.setCollection(flights);
        daoFlightCollection.saveCollectionToDB(daoFlightCollection.getCollection());
        List<Flight> flightCollection = daoFlightCollection.getCollectionFromDB();
        assertEquals(3, flightCollection.size());
    }



    @Test
    void checkGenerateFlightCollectionDBSuccess() {
        List<Flight> flightsGenerated = FlightGenerator.generateFlightCollection(5, 2);
        assertEquals(5, flightsGenerated.size());
    }



    @Test
    void checkGetAllFlightCollectionSuccess(){
        daoFlightCollection.setCollection(flights);
        assertEquals(3, daoFlightCollection.getCollection().size());
    }



    @Test
    void checkGetFlightByIndexSuccess() {
        daoFlightCollection.setCollection(flights);
        Flight flight = daoFlightCollection.getByIndex(0);
        assertEquals(1111, flight.getFlightID());
    }



    @Test
    void checkDeleteByIndexSuccess(){
        daoFlightCollection.setCollection(flights);
        boolean result = daoFlightCollection.deleteByIndex(0);
        assertTrue(result);
    }

    @Test
    void checkDeleteByIndexNotFondSuccess(){
        daoFlightCollection.setCollection(flights);
        boolean result = daoFlightCollection.deleteByIndex(10);
        assertFalse(result);
    }



    @Test
    void checkUpdateFlightCollectionSuccess() {
        daoFlightCollection.setCollection(flights);
        Flight flight = daoFlightCollection.getCollection().get(0);
        flight.setTotalPlaces(100);
        daoFlightCollection.updateCollection(flight);
        assertEquals(100, daoFlightCollection.getCollection().get(0).getTotalPlaces());
    }

    @Test
    void checkUpdateFlightCollectionAddNewFlightSuccess() {
        daoFlightCollection.setCollection(flights);
        Flight flight4 = new Flight(4444, "Sidney", 1636911127, 20, 15);
        daoFlightCollection.updateCollection(flight4);
        assertEquals(4, daoFlightCollection.getAllCollection().size());
    }



    @Test
    void saveCollectionToDB (){
        daoFlightCollection.setCollection(flights);
        assertEquals(3, daoFlightCollection.getCollection().size());

        Flight flight4 = new Flight(4444, "Sidney", 1636911127, 20, 15);
        Flight flight5 = new Flight(5555, "Madrid", 1636911127, 20, 15);
        daoFlightCollection.updateCollection(flight4);
        daoFlightCollection.updateCollection(flight5);

        daoFlightCollection.saveCollectionToDB(daoFlightCollection.getCollection());
        assertEquals(5, daoFlightCollection.getCollectionFromDB().size());

    }

}