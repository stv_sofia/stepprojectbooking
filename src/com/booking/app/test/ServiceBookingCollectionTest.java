package com.booking.app.test;

import com.booking.app.dao.DaoBookingCollection;
import com.booking.app.dao.DaoFlightCollection;
import com.booking.app.domain.models.Booking;
import com.booking.app.domain.models.Flight;
import com.booking.app.domain.models.Passenger;
import com.booking.app.domain.models.PassengerCollection;
import com.booking.app.services.ServiceBookingCollection;
import com.booking.app.services.ServiceFlightCollection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ServiceBookingCollectionTest {
    DaoBookingCollection daoBookingCollection = new DaoBookingCollection("testServiceBookingCollectionDB.data");
    ServiceBookingCollection serviceBookingCollection = new ServiceBookingCollection(daoBookingCollection);
    List<Booking> bookings = new ArrayList<>();


    @BeforeEach
    void setUp() {
        LocalDateTime dateTime = LocalDateTime.now();
        Flight flight1 = new Flight(1111, "Tokyo",
                dateTime.toEpochSecond(ZoneOffset.UTC), 20, 10);
        Flight flight2 = new Flight(2222, "London",
                dateTime.plusDays(10).toEpochSecond(ZoneOffset.UTC), 20, 18);
        Flight flight3 = new Flight(3333, "Adelaide",
                dateTime.plusDays(1).toEpochSecond(ZoneOffset.UTC), 20, 8);
        Passenger passenger1 = new Passenger("Sara", "Smit");
        Passenger passenger2 = new Passenger("John", "Smit");
        Passenger passenger3 = new Passenger("Bella", "Swan");
        Passenger passenger4 = new Passenger("Mark", "Swan");
        List<Passenger> passengersList1 = Arrays.asList(passenger1, passenger2);
        List<Passenger> passengersList2 = Arrays.asList(passenger3, passenger4);
        PassengerCollection passengersCollection1 = new PassengerCollection(passengersList1);
        PassengerCollection passengersCollection2 = new PassengerCollection(passengersList2);
        Booking booking1 = new Booking(flight1, passengersCollection1);
        Booking booking2 = new Booking(flight2, passengersCollection2);
        Booking booking3 = new Booking(flight3, passengersCollection1);
        bookings.add(booking1);
        bookings.add(booking2);
        bookings.add(booking3);
    }



    @Test
    void checkDeleteBookingByIDSuccess() {
        serviceBookingCollection.setBookingCollection(bookings);
        assertEquals(3, bookings.size());
        int bookingIdToDelete = serviceBookingCollection.getBookingCollection().get(0).getBookingID();
        serviceBookingCollection.deleteBookingByID(bookingIdToDelete);
        assertEquals(2, bookings.size());
    }

    @Test
    void checkDeleteBookingByIDNotFoundSuccess() {
        serviceBookingCollection.setBookingCollection(bookings);
        assertEquals(3, bookings.size());
        int bookingIdToDelete = 123456;
        boolean result = serviceBookingCollection.deleteBookingByID(bookingIdToDelete);
        assertEquals(3, bookings.size());
        assertFalse(result);
    }



    @Test
    void checkGetBookingListOfCertainPassengerSuccess() {
        serviceBookingCollection.setBookingCollection(bookings);
        List<Booking> bookingsFiltered =
                serviceBookingCollection.getBookingListOfCertainPassenger("Sara", "Smit");
        assertEquals(2, bookingsFiltered.size());
    }

    @Test
    void checkGetBookingListOfCertainPassengerNotFoundSuccess() {
        serviceBookingCollection.setBookingCollection(bookings);
        List<Booking> bookingsFiltered =
                serviceBookingCollection.getBookingListOfCertainPassenger("Klara", "Smit");
        assertEquals(0, bookingsFiltered.size());
    }



    @Test
    void checkGetBookingByBookingIDSuccess() {
        serviceBookingCollection.setBookingCollection(bookings);
        int bookingIdToFind = serviceBookingCollection.getBookingCollection().get(0).getBookingID();
        Booking booking = serviceBookingCollection.getBookingByBookingID(bookingIdToFind);
        assertNotNull(booking);
    }

    @Test
    void checkGetBookingByBookingIDNotFoundSuccess() {
        serviceBookingCollection.setBookingCollection(bookings);
        Booking booking = serviceBookingCollection.getBookingByBookingID(12345);
        assertNull(booking);
    }
}