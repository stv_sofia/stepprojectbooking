package com.booking.app.test;

import com.booking.app.dao.DaoFlightCollection;
import com.booking.app.domain.models.Flight;
import com.booking.app.services.ServiceFlightCollection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.*;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class ServiceFlightCollectionTest {
    DaoFlightCollection daoFlightCollection = new DaoFlightCollection("testServiceFlightCollectionDB.data");
    ServiceFlightCollection serviceFlightCollection = new ServiceFlightCollection(daoFlightCollection);
    List<Flight> flights = new ArrayList<>();


    @BeforeEach
    void setUp() {
        LocalDateTime dateTime = LocalDateTime.now();
        Flight flight1 = new Flight(1111, "Tokyo",
                dateTime.toEpochSecond(ZoneOffset.UTC), 20, 10);
        Flight flight2 = new Flight(2222, "London",
                dateTime.plusDays(10).toEpochSecond(ZoneOffset.UTC), 20, 18);
        Flight flight3 = new Flight(3333, "Adelaide",
                dateTime.plusDays(1).toEpochSecond(ZoneOffset.UTC), 20, 8);
        flights.add(flight1); flights.add(flight2); flights.add(flight3);
    }



    @Test
    void checkSortFlightsByDateSuccess() {
        serviceFlightCollection.setFlightCollection(flights);
        Flight flightForTest = flights.get(flights.size()-1);
        int fIdLastBefore = flights.get(flights.size()-1).getFlightID();
        assertEquals(2, flights.indexOf(flightForTest));

        List<Flight> flightsSort = serviceFlightCollection.sortFlightsByDate(flights);
        assertEquals(1, flightsSort.indexOf(flightForTest));
        int fIdLastAfter = flightsSort.get(flightsSort.size()-1).getFlightID();

        assertNotEquals(flights, flightsSort);
        assertNotEquals(fIdLastBefore, fIdLastAfter);
    }



    @Test
    void checkSortFlightsByDestinationSuccess() {
        serviceFlightCollection.setFlightCollection(flights);
        Flight flightForTest = flights.get(flights.size()-1);
        int indexOfFlightForTest = flights.indexOf(flightForTest);

        List<Flight> flightsSort = serviceFlightCollection.sortFlightsByDestination(flights);
        int indexOfFlightForTestAfterSort = flightsSort.indexOf(flightForTest);

        assertEquals(2, indexOfFlightForTest);
        assertEquals(0, indexOfFlightForTestAfterSort);
    }



    @Test
    void checkGetFlightByFlightIDSuccess() {
        serviceFlightCollection.setFlightCollection(flights);
        int flightIdExpected = serviceFlightCollection.getFlightCollection().get(0).getFlightID();
        int flightIdActual = serviceFlightCollection.getFlightByFlightID(flightIdExpected).getFlightID();
        assertEquals(flightIdExpected, flightIdActual);
    }

    @Test
    void checkGetFlightByFlightIDNoFoundSuccess() {
        serviceFlightCollection.setFlightCollection(flights);
        Flight flight = serviceFlightCollection.getFlightByFlightID(7777);
        assertNull(flight);
    }



    @Test
    void checkGetFlightsInOneDayPeriodSuccess() {
        serviceFlightCollection.setFlightCollection(flights);
        List<Flight> flightsInOneDayPeriod = serviceFlightCollection.getFlightsInOneDayPeriod();
        assertEquals(1, flightsInOneDayPeriod.size());
    }



    @Test
    void checkFindFlightsSuccess() {
        serviceFlightCollection.setFlightCollection(flights);
        Flight flight4 = new Flight(3333, "Milan",
                "21/11/2021 03:16:57 PM", 20, 8);
        serviceFlightCollection.updateFlightCollection(flight4);
        List<Flight> flights = serviceFlightCollection
                .findFlights("Milan", "21/11/2021",2);
        assertEquals(1, flights.size());
    }
}